/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_sign.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/02 15:27:47 by cumberto          #+#    #+#             */
/*   Updated: 2017/03/20 18:51:04 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int		unsigned_int_len(uintmax_t n)
{
	int		len;

	len = 0;
	if (n == 0)
		return (1);
	while (n > 0)
	{
		n = n / 10;
		len++;
	}
	return (len);
}

int		int_len(intmax_t n)
{
	int		len;

	len = 0;
	if (n == 0)
		return (1);
	while (n > 0)
	{
		n = n / 10;
		len++;
	}
	return (len);
}

char	*ft_itoa_sign(intmax_t n, t_print *elem)
{
	char			*str;
	int				size;
	intmax_t		num;

	if (n < 0)
		elem->sign = '-';
	if (n < -9223372036854775807)
		return (ft_strdup("9223372036854775808"));
	num = n < 0 ? -n : n;
	size = int_len(num);
	if ((str = ft_strnew(size)) == NULL)
		return (NULL);
	if (num == 0)
		str[0] = '0';
	while (num > 0)
	{
		str[size - 1] = 48 + (num % 10);
		num = num / 10;
		size--;
	}
	return (str);
}

char	*ft_itoa_unsign(uintmax_t n)
{
	char			*str;
	int				size;

	size = unsigned_int_len(n);
	if ((str = ft_strnew(size)) == NULL)
		return (NULL);
	if (n == 0)
		str[0] = '0';
	while (n > 0)
	{
		str[size - 1] = 48 + (n % 10);
		n = n / 10;
		size--;
	}
	return (str);
}
