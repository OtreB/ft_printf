/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_width.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/20 18:53:38 by cumberto          #+#    #+#             */
/*   Updated: 2017/03/24 04:56:45 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

void	print_char(char chr, int num)
{
	int cnt;

	cnt = 0;
	while (cnt < num)
	{
		write(1, &chr, 1);
		cnt++;
	}
}

void	print_str(t_print *elem, char *str, int size)
{
	if (elem->flag_m == 1)
	{
		write(1, str, size);
		print_char(elem->chr, elem->width - size);
	}
	else
	{
		print_char(elem->chr, elem->width - size);
		write(1, str, size);
	}
}
