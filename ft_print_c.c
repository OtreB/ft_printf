/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_c.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 13:11:38 by cumberto          #+#    #+#             */
/*   Updated: 2017/03/24 05:48:01 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int		ft_strlen_eol(char *str)
{
	int cnt;

	cnt = 0;
	if (str[cnt] == '\0')
		return (1);
	while (str[cnt])
		cnt++;
	return (cnt);
}

int		print_wchar(t_print *elem, wchar_t chr)
{
	wchar_t	str[2];
	char	*dst;
	int		size;

	str[0] = chr;
	str[1] = L'\0';
	dst = ft_conv_wchar(str);
	size = ft_strlen_eol(dst);
	if (elem->width > size)
	{
		print_str(elem, dst, size);
		free(dst);
		return (elem->width);
	}
	size = ft_strlen_eol(dst);
	write(1, dst, size);
	free(dst);
	return (size);
}

int		ft_print_chr(t_print *elem, va_list arg)
{
	char	str[2];

	if (elem->spec == 'C' || elem->mod[0] == 'l')
		return (print_wchar(elem, va_arg(arg, wchar_t)));
	else
	{
		if (elem->spec != 'c')
			str[0] = elem->spec;
		else
			str[0] = va_arg(arg, int);
		str[1] = '\0';
	}
	print_str(elem, str, 1);
	if (elem->width > 1)
		return (elem->width);
	else
		return (1);
}
