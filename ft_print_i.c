/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/06 11:08:45 by cumberto          #+#    #+#             */
/*   Updated: 2017/03/24 04:54:52 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

void	print_str_int(t_print *elem, char *str, int size, int sign)
{
	int		max;

	if (elem->sign != 0 && (elem->flag_m == 1 || elem->flag_z == 1))
		write(1, &elem->sign, 1);
	max = elem->prec > size ? elem->prec : size;
	if (elem->width > max)
	{
		if (elem->flag_m == 1)
		{
			if (elem->prec > size)
				print_char('0', elem->prec - size);
			write(1, str, size);
			print_char(' ', elem->width - max - sign);
			return ;
		}
		else
			print_char(elem->chr, elem->width - max - sign);
	}
	if (elem->sign != 0 && elem->flag_z != 1 && elem->flag_z != 1)
		write(1, &elem->sign, 1);
	if (elem->prec > size)
		print_char('0', elem->prec - size);
	write(1, str, size);
}

int		ft_print_int(t_print *elem, va_list *arg)
{
	char	*str;
	int		sign;
	int		size;

	sign = 1;
	str = ft_itoa_sign(manage_signed_mod(elem, *arg), elem);
	if (str[0] == '0' && elem->prec == 0)
		str = "";
	if (elem->flag_p == 1 && elem->sign != '-')
		elem->sign = '+';
	else if (elem->flag_s == 1 && elem->sign != '-')
		elem->sign = ' ';
	else if (elem->sign != '-')
		sign = 0;
	print_str_int(elem, str, (size = ft_strlen(str)), sign);
	if (elem->prec > size)
		size = elem->prec;
	if (elem->width > size)
		size = elem->width;
	if (size == (int)ft_strlen(str) || size == elem->prec)
		size += sign;
	if (str[0] != '\0')
		free(str);
	return (size);
}
