/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 21:23:00 by cumberto          #+#    #+#             */
/*   Updated: 2016/12/05 18:57:00 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(const char *str)
{
	char	*dst;
	size_t	start;
	size_t	end;
	int		i;

	if (str == NULL)
		return (NULL);
	start = 0;
	i = 0;
	while (str[start] == '\t' || str[start] == '\n' || str[start] == ' ')
		start++;
	if (start == ft_strlen(str))
		return (ft_strnew(0));
	end = ft_strlen(str) - 1;
	while (str[end] == '\t' || str[end] == '\n' || str[end] == ' ')
		end--;
	if ((dst = ft_strnew(end - start + 1)) == NULL)
		return (NULL);
	while (start <= end)
		dst[i++] = str[start++];
	dst[i] = '\0';
	return (dst);
}
