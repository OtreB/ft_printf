/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_conv_wchar.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/22 15:01:58 by cumberto          #+#    #+#             */
/*   Updated: 2017/03/24 05:59:59 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		wchar_size(wchar_t chr)
{
	if (chr < 0x7f)
		return (1);
	else if (chr < 0x7ff)
		return (2);
	else if (chr < 0xffff)
		return (3);
	else
		return (4);
}

char	*copy_wchar(wchar_t *chr, char *str, int cnt, int i)
{
	while (chr[++i] != L'\0')
	{
		if (chr[i] < 0x7f)
			str[cnt] = (chr[i]);
		else if (chr[i] < 0x7ff)
		{
			str[cnt++] = ((chr[i] >> 6) | 0xc0);
			str[cnt] = ((chr[i] | 0x80) & 0xbf);
		}
		else if (chr[i] < 0xffff)
		{
			str[cnt++] = ((chr[i] >> 12) | 0xe0);
			str[cnt++] = (((chr[i] >> 6) | 0x80) & 0xbf);
			str[cnt] = ((chr[i] | 0x80) & 0xbf);
		}
		else
		{
			str[cnt++] = (((chr[i] >> 18) | 0xf0) & 0xf7);
			str[cnt++] = (((chr[i] >> 12) | 0x80) & 0xbf);
			str[cnt++] = (((chr[i] >> 6) | 0x80) & 0xbf);
			str[cnt] = ((chr[i] | 0x80) & 0xbf);
		}
		cnt++;
	}
	return (str);
}

char	*ft_conv_wchar(wchar_t *str)
{
	int		size;
	char	*dst;
	int		cnt;

	cnt = 0;
	size = 0;
	while (str[cnt])
	{
		size += wchar_size(str[cnt]);
		cnt++;
	}
	dst = ft_strnew(size);
	dst = copy_wchar(str, dst, 0, -1);
	return (dst);
}
