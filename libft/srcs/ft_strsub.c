/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 18:54:28 by cumberto          #+#    #+#             */
/*   Updated: 2016/11/29 21:14:35 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(const char *s1, unsigned int start, size_t len)
{
	char *str;

	if (s1 != NULL)
	{
		if ((str = (char*)malloc(sizeof(*str) * len + 1)) == NULL)
			return (NULL);
		ft_memcpy((void*)str, &s1[start], len);
		str[len] = '\0';
		return (str);
	}
	return (NULL);
}
