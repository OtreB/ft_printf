/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_arg.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/13 15:41:37 by cumberto          #+#    #+#             */
/*   Updated: 2017/03/27 15:35:05 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int		print_arg(t_print *elem, va_list *arg)
{
	if (elem->spec == 'd' || elem->spec == 'i' || elem->spec == 'D')
		return (ft_print_int(elem, arg));
	else if (elem->spec == 'X' || elem->spec == 'x')
		return (ft_print_hex(elem, *arg));
	else if (elem->spec == 's' || elem->spec == 'S')
		return (ft_print_str(elem, *arg));
	else if (elem->spec == '%')
		return (ft_print_per(elem));
	else if (elem->spec == 'o' || elem->spec == 'O')
		return (ft_print_oct(elem, arg));
	else if (elem->spec == 'u' || elem->spec == 'U')
		return (ft_print_und(elem, arg));
	else if (elem->spec == 'c' || elem->spec == 'C')
		return (ft_print_chr(elem, *arg));
	else if (elem->spec == 'p')
		return (ft_print_pnt(elem, *arg));
	else if (elem->spec == '\0')
		return (0);
	return (ft_print_chr(elem, *arg));
}
