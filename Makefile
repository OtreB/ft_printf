# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cumberto <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/11/15 10:10:44 by cumberto          #+#    #+#              #
#    Updated: 2017/03/30 21:38:29 by cumberto         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

LIB_SRC  = ft_memset.c \
		   ft_bzero.c \
		   ft_memcpy.c \
		   ft_memccpy.c \
		   ft_memmove.c \
		   ft_putchar.c \
		   ft_putstr.c \
		   ft_strcpy.c \
		   ft_strlen.c \
		   ft_strdup.c \
		   ft_strncpy.c \
		   ft_strcat.c \
		   ft_strncat.c \
		   ft_strlcat.c \
		   ft_strchr.c \
		   ft_strrchr.c \
		   ft_strstr.c \
		   ft_strnstr.c \
		   ft_strcmp.c \
		   ft_strncmp.c \
		   ft_atoi.c \
		   ft_isalpha.c \
		   ft_isdigit.c \
		   ft_isalnum.c \
		   ft_isascii.c \
		   ft_isprint.c \
		   ft_toupper.c \
		   ft_tolower.c \
		   ft_memalloc.c \
		   ft_memdel.c \
		   ft_strnew.c \
		   ft_strdel.c \
		   ft_strclr.c \
		   ft_striter.c \
		   ft_striteri.c \
		   ft_strmap.c \
		   ft_strmapi.c \
		   ft_strequ.c \
		   ft_strnequ.c \
		   ft_putendl.c \
		   ft_putnbr.c \
		   ft_putchar_fd.c \
		   ft_putstr_fd.c \
		   ft_putendl_fd.c \
		   ft_putnbr_fd.c \
		   ft_strsub.c \
		   ft_strjoin.c \
		   ft_strtrim.c \
		   ft_memcmp.c \
		   ft_itoa.c \
		   ft_strsplit.c \
		   ft_lstnew.c \
		   ft_lstdelone.c \
		   ft_lstdel.c \
		   ft_lstadd.c \
		   ft_lstiter.c \
		   ft_lstmap.c \
		   ft_strrev.c \
		   ft_isspace.c \
		   ft_isblank.c \
		   ft_ispunct.c  \
		   ft_get_next_line.c \
		   ft_putwchar.c \
		   ft_putwchar_str.c \
		   ft_conv_wchar.c \

PRINTF_SRC = ft_printf.c \
			 ft_print_i.c \
			 ft_itoa_sign.c \
			 manage_modifier.c \
			 ft_print_s.c \
			 ft_print_x.c \
			 ft_print_per.c \
			 print_arg.c \
			 ft_print_o.c \
			 ft_print_u.c \
			 ft_print_c.c \
			 ft_print_p.c \
			 ft_print_width.c \
			 ft_read_format.c \

PRINTF_OBJ = $(PRINTF_SRC:.c=.o)

LIB_OBJ = $(LIB_SRC:.c=.o)

CFLAGS = -Werror -Wall -Wextra

NAME = libftprintf.a

all: $(NAME)

$(NAME): libft/libft.a
	@gcc $(CFLAGS) -c $(PRINTF_SRC) -I ./includes/ -I ./libft/includes
	@ar rc $(NAME) $(addprefix libft/obj/, $(LIB_OBJ)) $(PRINTF_OBJ);
	@ranlib $(NAME)

libft/libft.a:
	@make -C ./libft/

clean:
	@rm -f $(LIB_OBJ) $(PRINTF_OBJ)
	@make -C libft clean

fclean: clean
	@rm -f $(NAME)
	@make -C libft fclean

test2: $(NAME)
	@gcc main.c -g $(PRINTF_SRC) -L ./libft/ -lft -I ./includes/ \
		-I ./libft/includes

re: fclean all
