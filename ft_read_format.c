/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read_format.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/24 04:50:22 by cumberto          #+#    #+#             */
/*   Updated: 2017/03/24 06:23:09 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int		find_flags(const char *str, t_print *elem)
{
	int i;

	i = 0;
	while (g_table[(int)str[i]] != 2)
	{
		if (str[i] == '0')
			elem->flag_z = 1;
		else if (str[i] == '+')
			elem->flag_p = 1;
		else if (str[i] == '-')
			elem->flag_m = 1;
		else if (str[i] == ' ')
			elem->flag_s = 1;
		else if (str[i] == '#')
			elem->flag_h = 2;
		else
			break ;
		i++;
	}
	return (i);
}

int		find_prec(const char *str, t_print *elem, va_list arg)
{
	int i;

	i = 1;
	if (str[1] == '*')
	{
		elem->prec = va_arg(arg, int);
		return (2);
	}
	elem->prec = ft_atoi(&str[i]);
	while (ft_isdigit(str[i]) == 1)
		i++;
	return (i);
}

int		find_width(const char *str, t_print *elem, va_list arg)
{
	int i;

	i = 0;
	if (str[0] == '*')
	{
		if ((elem->width = va_arg(arg, int)) < 0)
		{
			elem->width = -elem->width;
			elem->flag_m = 1;
		}
		return (1);
	}
	elem->width = ft_atoi(&str[i]);
	while (ft_isdigit(str[i]) == 1)
		i++;
	return (i);
}

void	read_format(const char *str, t_print *elem, va_list *arg)
{
	int i;
	int k;

	k = 0;
	elem->chr = ' ';
	i = find_flags(str, elem);
	elem->width = -1;
	if (ft_isdigit(str[i]) || str[i] == '*')
		i += find_width(&str[i], elem, *arg);
	elem->prec = -1;
	if (str[i] == '.')
		i += find_prec(&str[i], elem, *arg);
	while (str[i] == 'h' || str[i] == 'l' || str[i] == 'z' || str[i] == 'j')
	{
		elem->mod[k] = str[i];
		i++;
		k++;
	}
	elem->spec = str[i];
	if (elem->prec > 0 || elem->width < 0 || elem->flag_m == 1)
		elem->flag_z = 0;
	if (elem->flag_z == 1)
		elem->chr = '0';
}
