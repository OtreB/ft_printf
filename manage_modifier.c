/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_mod.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/06 16:24:47 by cumberto          #+#    #+#             */
/*   Updated: 2017/03/14 18:17:36 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

intmax_t	manage_signed_mod(t_print *elem, va_list ap)
{
	intmax_t i;

	if (elem->mod[0] == 'l' || elem->spec == 'D')
		i = (intmax_t)va_arg(ap, long);
	else if (elem->mod[0] == 'h' && elem->mod[1] == 'h')
		i = (intmax_t)(char)va_arg(ap, int);
	else if (elem->mod[0] == 'h')
		i = (intmax_t)(short)va_arg(ap, int);
	else if (elem->mod[0] == 'l' && elem->mod[1] == 'l')
		i = (intmax_t)va_arg(ap, long long);
	else if (elem->mod[0] == 'j')
		i = va_arg(ap, intmax_t);
	else if (elem->mod[0] == 'z')
		i = (intmax_t)va_arg(ap, ssize_t);
	else
		i = (intmax_t)va_arg(ap, int);
	return (i);
}

uintmax_t	manage_unsigned_mod(t_print *elem, va_list ap)
{
	uintmax_t i;

	if (elem->mod[0] == 'l' || elem->spec == 'U' || elem->spec == 'O')
		i = (uintmax_t)va_arg(ap, unsigned long);
	else if (elem->mod[0] == 'h' && elem->mod[1] == 'h')
		i = (uintmax_t)(unsigned char)va_arg(ap, unsigned int);
	else if (elem->mod[0] == 'h')
		i = (uintmax_t)(unsigned short)va_arg(ap, unsigned int);
	else if (elem->mod[0] == 'l' && elem->mod[1] == 'l')
		i = (uintmax_t)va_arg(ap, unsigned long long);
	else if (elem->mod[0] == 'j')
		i = va_arg(ap, uintmax_t);
	else if (elem->mod[0] == 'z')
		i = (uintmax_t)va_arg(ap, size_t);
	else
		i = (uintmax_t)va_arg(ap, unsigned int);
	return (i);
}
