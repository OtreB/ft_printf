/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_1.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/02 11:22:56 by cumberto          #+#    #+#             */
/*   Updated: 2017/03/27 16:09:01 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int		check_format(const char *str, t_print elem)
{
	int cnt;

	cnt = 1;
	if (str[cnt] == '\0')
		return (0);
	while (str[cnt] != elem.spec)
		cnt++;
	return (cnt);
}

int		ft_printf(const char *format, ...)
{
	t_print	elem;
	va_list	arg;
	int		i;
	int		num;

	num = 0;
	i = 0;
	va_start(arg, format);
	while (format[i] != '\0')
	{
		if (format[i] != '%')
			ft_putchar(format[i]);
		else
		{
			ft_bzero(&elem, sizeof(t_print));
			read_format(&format[i + 1], &elem, &arg);
			num += print_arg(&elem, &arg) - 1;
			i += check_format(&format[i], elem);
		}
		i++;
		num++;
	}
	return (num);
}
