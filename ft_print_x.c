/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_x.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/06 17:42:26 by cumberto          #+#    #+#             */
/*   Updated: 2017/03/24 05:52:29 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

char	*convert_hex(t_print *elem, uintmax_t num)
{
	char		*str;
	int			cnt;
	uintmax_t	tmp;

	cnt = 1;
	tmp = num;
	while ((tmp = tmp / 16) != 0)
		cnt++;
	str = ft_strnew(cnt);
	while (cnt--)
	{
		if ((tmp = (num % 16) + 48) > 57)
		{
			if (elem->spec == 'X')
				tmp += 7;
			else
				tmp += 39;
		}
		str[cnt] = tmp;
		num = num / 16;
	}
	return (str);
}

void	print_str_hex(t_print *elem, char *str, int size, char *hashtag)
{
	int max;

	if (elem->flag_h == 2 && (elem->flag_m == 1 || elem->flag_z == 1))
		write(1, hashtag, 2);
	max = elem->prec > size ? elem->prec : size;
	if (elem->width > max)
	{
		if (elem->flag_m == 1)
		{
			if (elem->prec > size)
				print_char('0', elem->prec - size);
			write(1, str, size);
			print_char(' ', elem->width - max - elem->flag_h);
			return ;
		}
		else
			print_char(elem->chr, elem->width - max - elem->flag_h);
	}
	if (elem->flag_h == 2 && elem->flag_z != 1 && elem->flag_z != 1)
		write(1, hashtag, 2);
	if (elem->prec > size)
		print_char('0', elem->prec - size);
	write(1, str, size);
}

int		ft_print_hex(t_print *elem, va_list arg)
{
	char		*str;
	char		*hashtag;
	int			size;
	uintmax_t	num;

	if ((num = manage_unsigned_mod(elem, arg)) == 0)
		elem->flag_h = 0;
	if (num == 0 && elem->prec == 0)
		str = "";
	else
		str = convert_hex(elem, num);
	hashtag = "0x";
	if (elem->spec == 'X')
		hashtag = "0X";
	print_str_hex(elem, str, (size = ft_strlen(str)), hashtag);
	if (elem->prec > size)
		size = elem->prec;
	if (elem->width > size)
		size = elem->width;
	if (size == (int)ft_strlen(str) || size == elem->prec)
		size += elem->flag_h;
	if (str[0] != '\0')
		free(str);
	return (size);
}
