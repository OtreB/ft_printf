/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/06 16:52:54 by cumberto          #+#    #+#             */
/*   Updated: 2017/03/24 06:22:42 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# include <stdarg.h>
# include "libft.h"
# include "stdint.h"
# include <wchar.h>
# include <stdio.h>

int					ft_printf(const char *format, ...);

typedef struct		s_print
{
	int				sign;
	int				flag_z;
	int				flag_h;
	int				flag_m;
	int				flag_s;
	int				flag_p;
	int				width;
	int				prec;
	char			mod[2];
	char			spec;
	char			chr;
}					t_print;

int					print_arg(t_print *elem, va_list *arg);
char				*ft_itoa_sign(intmax_t n, t_print *elem);
char				*ft_itoa_unsign(uintmax_t n);
intmax_t			manage_signed_mod(t_print *elem, va_list ap);
uintmax_t			manage_unsigned_mod(t_print *elem, va_list ap);
int					ft_print_str(t_print *elem, va_list arg);
int					ft_print_hex(t_print *elem, va_list arg);
int					ft_print_int(t_print *elem, va_list *arg);
int					ft_print_per(t_print *elem);
int					ft_print_oct(t_print *elem, va_list *arg);
int					ft_print_und(t_print *elem, va_list *arg);
int					ft_print_chr(t_print *elem, va_list arg);
int					ft_print_pnt(t_print *elem, va_list arg);
void				print_str(t_print *elem, char *str, int size);
void				print_char(char chr, int num);
void				print_str_und(t_print *elem, char *str, int size, int max);
void				read_format(const char *str, t_print *elem, va_list *arg);

static char		g_table[127] =
{
	['#'] = 1,
	[' '] = 1,
	['%'] = 2,
	['*'] = 1,
	['+'] = 1,
	['-'] = 1,
	['.'] = 1,
	['0'] = 1,
	['1'] = 1,
	['2'] = 1,
	['3'] = 1,
	['4'] = 1,
	['5'] = 1,
	['6'] = 1,
	['7'] = 1,
	['8'] = 1,
	['9'] = 1,
	['h'] = 1,
	['l'] = 1,
	['j'] = 1,
	['z'] = 1,
	['s'] = 2,
	['S'] = 2,
	['p'] = 2,
	['d'] = 2,
	['D'] = 2,
	['i'] = 2,
	['I'] = 2,
	['o'] = 2,
	['O'] = 2,
	['u'] = 2,
	['U'] = 2,
	['x'] = 2,
	['X'] = 2,
	['c'] = 2,
	['C'] = 2,
};

#endif
